Welcome to Migration Example
===================


Hey! We are going to send this repository from **Bitbucket** to **GitLab**.

**Gitlab**
Provides Git repository management, code reviews, issue tracking, activity feeds and wikis. GitLab itself is also free software.

 - Feature rich: Git repository management, code reviews, issue tracking, activity feeds and wikis. 
 - It comes with GitLab CI for continuous integration and delivery. 
 - Open Source: MIT licensed, community driven, 700+ contributors, inspect and modify the source, easy to integrate into your infrastructure.
 - Scalable: support 25,000 users on one server or a highly available active/active cluster.
 

To use GitLab without installing it, check out GitLab.com Note that only self-hosted GitLab will let you integrate with your LDAP.